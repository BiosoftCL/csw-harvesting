<?

namespace Geonodo\CSWHarvesting\Utils;

class Cron
{
    public static function convertCronToObject($seed)
    {
        $frequency = $seed->frequency;
        $cron = $seed->cron;
        switch ($frequency) {
                // dailyAt
            case 8:
                $seed->first_hour_daily_at =  explode(' ', $cron)[1] . ":" . explode(' ', $cron)[0];
                break;
                // twiceDaily
            case 9:
                $times = explode(' ', $cron)[1];
                $seed->first_hour_twice_daily = explode(',', $times)[0];
                $seed->second_hour_twice_daily = explode(',', $times)[1];
                break;
                // weeklyOn
            case 11:
                $seed->first_hour_weekly_on =  explode(' ', $cron)[1] . ":" . explode(' ', $cron)[0];
                $seed->weekday_weekly_on = explode(' ', $cron)[4];
                break;
                // monthlyOn
            case 13:
                $seed->first_hour_monthly_on =  explode(' ', $cron)[1] . ":" . explode(' ', $cron)[0];
                $seed->day_monthly_on = (int) explode(' ', $cron)[2] - 1;
                break;
        }
        return $seed;
    }

    /**
     * Permite obtener la configuracion para un cron
     * en base a una frecuencia particular
     *
     * @param $request
     * @return string
     */
    public static function convertRequestToCron($request)
    {
        $frequencyOption = $request->frequencyOption;
        $cron = '';
        switch ($frequencyOption) {
                // everyMinute
            case 1:
                $cron = '* * * * *';
                break;
                // everyFiveMinutes
            case 2:
                $cron = '*/5 * * * *';
                break;
                // everyTenMinutes
            case 3:
                $cron = '*/10 * * * *';
                break;
                // everyFifteenMinutes
            case 4:
                $cron = '*/15 * * * *';
                break;
                // everyThirtyMinutes
            case 5:
                $cron = '*/30 * * * *';
                break;
                // hourly
            case 6:
                $cron = '0 * * * *';
                break;
                // daily
            case 7:
                $cron = '0 0 * * *';
                break;
                // dailyAt
            case 8:
                $time = Cron::getHourAndMinute($request->first_hour_daily_at);
                $cron = $time['minute'] . ' ' . $time['hour'] . ' * * *';
                break;
                // twiceDaily
            case 9:
                $timeOne = $request->first_hour_twice_daily;
                $timeTwo = $request->second_hour_twice_daily;
                $cron = '0 ' . $timeOne . ',' . $timeTwo . ' * * *';
                break;
                // weekly
            case 10:
                $cron = '0 0 * * 1';
                break;
                // weeklyOn
            case 11:
                $time = Cron::getHourAndMinute($request->first_hour_weekly_on);
                $weekday = $request->weekday_weekly_on;
                $cron = $time['minute'] . ' ' . $time['hour'] . ' * * ' . $weekday;
                break;
                // monthly
            case 12:
                $cron = '0 0 1 * *';
                break;
                // monthlyOn
            case 13:
                $time = Cron::getHourAndMinute($request->first_hour_monthly_on);
                $day = 1 + (int) $request->day_monthly_on;
                $cron = $time['minute'] . ' ' . $time['hour'] . ' ' . $day . ' * *';
                break;
                // quarterly
            case 14:
                $cron = '0 0 1 */3 *';
                break;
        }
        return $cron;
    }

    public static function getHourAndMinute($time)
    {
        return [
            'hour' =>  explode(':', $time)[0],
            'minute' => explode(':', $time)[1]
        ];
    }
}

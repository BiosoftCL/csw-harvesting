<?php

namespace Geonodo\CSWHarvesting;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class HarvestRecord extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pkg_cswharvest_records';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public function seed()
    {
        return $this->belongsTo(Seed::class, 'source_id');
    }

    public function status()
    {
        if ($this->updated_at != null)
            return 'finalized';
        return 'process';
    }

    public function getIcon()
    {

        if ($this->errors != null)
            return 'glyphicon-remove-circle';
        if ($this->updated_at != null)
            return 'glyphicon-ok-circle';
        return 'glyphicon-refresh';
    }

    public function duration()
    {
        return $this->created_at->diff($this->updated_at)->format('%H:%I:%S');
    }

    public function initiated()
    {
        return $this->created_at->since();
    }
}
<?php

namespace Geonodo\CSWHarvesting;

use Illuminate\Database\Eloquent\Model;

class Seed extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pkg_cswharvest_seeds';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'url', 'cron', 'frequency', 'instance_id'];
}

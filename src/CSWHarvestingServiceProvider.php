<?php

namespace Geonodo\CSWHarvesting;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;
use Geonodo\CSWHarvesting\Console\Commands\CswHarvesting;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;


class CSWHarvestingServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();
        $this->registerRoutes();
        $this->registerResources();
        $this->registerTranslations();
        $this->loadMigrations();
        $this->publishAssets();
        $this->cronJob();
        $this->registerCommand();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Setup the configuration.
     *
     * @return void
     */
    protected function publishConfig()
    {
        $this->publishes([
            __DIR__ . '/../config/csw-harvesting.php' => config_path('csw-harvesting.php'),
        ]);
    }

    /**
     * Register the resources.
     *
     * @return void
     */
    protected function registerResources()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'csw-harvesting');
    }

    /**
     * Register the routes and breadcrumbs.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group([
            'prefix'     => '/w/{instance_code}/csw-har',
            'namespace'  => 'Geonodo\CSWHarvesting\Http\Controllers',
            'middleware' => ['web', 'auth', 'instance_user', 'scope-bouncer']
        ], function () {
            // Routes
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

            // Breadcrumbs
            $this->loadRoutesFrom(__DIR__ . '/../routes/breadcrumbs.php');
        });
    }

    /**
     * Register the translations.
     *
     * @return void
     */
    protected function registerTranslations()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'csw-harvesting');
    }

    /**
     * Register the translations.
     *
     * @return void
     */
    protected function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    /**
     * Publish the assets
     */
    protected function publishAssets()
    {
        $this->publishes([
            __DIR__ . '/../resources/assets/js/components' => base_path('resources/assets/js/vendor/csw-harvesting'),
        ], 'assets');
    }

    protected function cronJob()
    {
        try {
            $cronJobs = Seed::all();
            $this->app->booted(function () use($cronJobs) {
                $schedule = $this->app->make(Schedule::class);
                foreach ($cronJobs as $cronJob) {
                    $schedule->command('csw:harvest ' . $cronJob->id)->cron($cronJob->cron)->runInBackground();
                }
            });
        } catch ( QueryException $e) {
            Log::error('No existe la tabla Seed, puede que no alla ejecutado artisan migrate ');
        }
    }

    protected function registerCommand()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CswHarvesting::class,
            ]);
        }
    }
}

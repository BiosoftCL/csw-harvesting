<?php

namespace Geonodo\CSWHarvesting\Http\Controllers;

use Geonodo\CSWHarvesting\HarvestRecord;
use Geonodo\CSWHarvesting\Http\Requests\StoreCSW;
use Geonodo\CSWHarvesting\Http\Requests\UpdateCSW;
use Geonodo\Domain\Model\Instance;
use Geonodo\CSWHarvesting\Utils\Cron;
use Cron\CronExpression;

class HarvertRecordController
{
    /**
     * Download Log File in .log
     *
     * @param Instance $instance
     * @param HarvestRecord $record
     *
     * @return mixed
     */
    public function export(Instance $instance, HarvestRecord $record)
    {
        $errors = $record->errors;
        $name = $record->seed->name . ' ' . $record->created_at . '.log';

        //offer the content of txt as a download (logs.txt)
        $headers = ['Content-type' => 'text/plain', 'Content-Disposition' => sprintf('attachment; filename=' . $name)];

        return response($errors, 200, $headers);
    }
}

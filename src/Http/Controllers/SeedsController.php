<?php

namespace Geonodo\CSWHarvesting\Http\Controllers;

use Geonodo\CSWHarvesting\Seed;
use Geonodo\CSWHarvesting\HarvestRecord;
use Geonodo\CSWHarvesting\Http\Requests\StoreCSW;
use Geonodo\CSWHarvesting\Http\Requests\UpdateCSW;
use Geonodo\Domain\Model\Instance;
use Geonodo\CSWHarvesting\Utils\Cron;
use Cron\CronExpression;
use phpDocumentor\Reflection\DocBlock\Tags\See;

class SeedsController
{
    public function index()
    {
        $collection = Seed::paginate(15);

        return view('csw-harvesting::csw.index', compact('collection'));
    }

    public function show(Instance $instance, $id)
    {
        $csw = Seed::where('id', $id)->first();
        if (!$csw) {
            abort(404);
        }

        $collection = HarvestRecord::where('source_id', $id)->orderBy('created_at', 'desc')->paginate(15);

        return view('csw-harvesting::csw.show', compact('csw', 'collection'));
    }

    public function create()
    {
        return view('csw-harvesting::csw.create');
    }

    public function store(StoreCSW $request, Instance $instance)
    {
        $cron = Cron::convertRequestToCron($request);
        // dd(CronExpression::factory($cron)->__toString());
        Seed::create([
            'name' => $request->name,
            'url' => $request->url,
            'cron' => $cron,
            'frequency' => $request->frequencyOption,
            'instance_id' => $instance->id
        ]);

        flash()->success(__('csw-harvesting::common.create.success_msg'))->important();
        return redirect()->route('csw-harvesting::csw.index', $instance);
    }

    public function edit(Instance $instance, $id)
    {
        $seed = Seed::where('id', $id)->first();
        if (!$seed) {
            abort(404);
        }
        $seed = Cron::convertCronToObject($seed);
        return view('csw-harvesting::csw.edit', compact('seed'));
    }

    public function update(UpdateCSW $request, Instance $instance, $id)
    {
        $csw = Seed::where('id', $id)->first();
        if (!$csw) {
            abort(404);
        }
        $cron = Cron::convertRequestToCron($request);
        $csw->update([
            'name' => $request->name,
            'url' => $request->url,
            'instance_id' => $instance->id,
            'frequency' => $request->frequencyOption,
            'cron' => $cron,
        ]);
        flash()->success(__('csw-harvesting::common.edit.success_msg'))->important();
        return redirect()->route('csw-harvesting::csw.index', $instance);
    }

    public function destroy(Instance $instance, $id)
    {
        try {
            Seed::destroy($id);
        } catch (\Exception $e) {
            logger('\Geonodo\CSWHarvesting\Http\Controllers\FooController@destroy', ['csw' => $id, 'msg' => $e->getMessage()]);

            flash(__('csw-harvesting::common.deleted_error'), 'error');
        }
        return redirect()->back();
    }

    public function retry(Instance $instance, $id)
    {
        // \Artisan::call('config:cache');
        // \Artisan::call('csw:harvest', ['seed' => $id]);
        return redirect()->back();
    }
}

<?php

namespace Geonodo\CSWHarvesting\Console\Commands;

use Geonodo\CSWHarvesting\Seed;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;


class CswHarvesting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csw:harvest {seed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        $seed = Seed::where('id', $this->argument('seed'))->first();
        Log::info('http://192.168.1.3:5002/cosecha/'. $seed->id);

        $promise = $client->getAsync('http://csw-harvest/cosecha/' . $seed->id)
        // $promise = $client->getAsync('http://csw-harvest/test')
        // $promise = $client->getAsync('http://192.168.1.3:5002/cosecha/' . $seed->id)
        ->then(function ($response) use($seed){
            $statusCode = $response->getStatusCode();
            $body = $response->getBody()->getContents();
            Log::info("Respuesta para ". $seed->id.  ": Estado: ".$statusCode. " Respuesta: " . $body);
        },
        function ($exception){
            $body = $exception->getMessage();
            Log::info("Error Respuesta: " . $body);
        });
        $promise->wait();
        Log::info('Paso: ' . $seed->id);

    }
}

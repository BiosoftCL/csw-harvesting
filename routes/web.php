<?php

use GuzzleHttp\Client;

/*
 * Skeleton routes
 * ------------------
 */
// Route::resource('foo', 'HarvestingController', ['only' => ['index', 'create', 'store', 'destroy'], 'as' => 'csw-harvesting']);

Route::group(['as' => 'csw-harvesting::'], function () {
	Route::resource('csw', 'SeedsController');
	Route::get('harvest/{record}/export', 'HarvertRecordController@export')->name('csw.harvest.export');
	Route::get('harvest/{record}/retry', 'SeedsController@retry')->name('csw.harvest.retry');
});
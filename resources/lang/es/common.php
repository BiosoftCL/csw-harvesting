<?php

return [
    'menu' => [
        'title' => 'Cosecha de CSW',
        'details' => 'Registro de Catálogos de Servicios para la Web'
    ],
    'index' => [
        'title' => 'Catálogos de Servicios para la Web para la cosecha',
        'title_breadcrumb' => 'Recursos CSW',
        'new_btn' => 'Nuevo',
    ],
    'cron' => [
        'cron_section_title' => 'CRON',
            /** CRON FREQUENCY **/
        'minutes_title_section' => 'Minutos',
        'hours_title_section' => 'Horas',
        'days_title_section' => 'Días',
        'weeks_title_section' => 'Semanas',
        'months_title_section' => 'Meses',
        'everyMinute' => 'Cada minuto ',
        'everyFiveMinutes' => 'Cada 5 minutos',
        'everyTenMinutes' => 'Cada 10 minutos',
        'everyFifteenMinutes' => 'Cada 15 minutos ',
        'everyThirtyMinutes' => 'Cada 30 minutos ',
        'hourly' => 'Cada 1 hora',
        'daily' => 'Todos los días a medianoche',
        'dailyAt' => 'Todos los días a las',
        'twiceDaily' => 'Todos los días a las',
        'between_hours' => 'y a las',
        'weekly' => 'Todas las semanas',
        'weeklyOn' => 'Todos las semanas, el día',
        'hour_option' => 'a las',
        'monthly' => 'Todos los meses',
        'monthlyOn' => 'Todos los meses, el día',
        'quarterly' => 'Cada trimestre',
        'criterion-title' => 'Criterio de sincronización',
        'all_non-existent_records' => 'Insertar todos los registros inexistentes en tabla de recepción que cumplan con reglas de tabla receptora',
        'insert_all-new_records' => 'Insertar todos los nuevos registros detectados en la tabla publicadora',
        'uri_error' => 'La URI se encuentra corrupta o no posees la llave de acceso correcta',
        'monday' => 'Lunes',
        'tuesday' => 'Martes',
        'wednesday' => 'Miércoles',
        'thursday' => 'Jueves',
        'friday' => 'Viernes',
        'saturday' => 'Sábado',
        'sunday' => 'Domingo',
    ],
    'create' => [
        'title' => 'Nuevo servidor CSW para cosecha',
        'title_breadcrumb' => 'Nuevo servidor CSW',
        'success_msg' => 'Se ha creado el registro correctamente',
    ],
    'edit' => [
        'title' => 'Editar servidor CSW',
        'title_breadcrumb' => 'Editar servidor CSW',
        'edit_btn' => 'Editar',
        'success_msg' => 'Se ha editado el registro correctamente',
    ],
    'show' => [
        'title_breadcrumb' => 'Ver registro',
        'retry' => 'Reintentar'
    ],
    'csw' => [
        'fields' => [
            'name' => 'Nombre',
            'url' => 'URL',
            'cron' => 'Periodo',
        ],
        'actions' => [
            'delete' => 'Eliminar csw',
            'delete_confirm' => '¿Desea eliminar un registro?'
        ],
        'record' => [
            'title' => 'Resultados',
            'status' => 'Estado',
            'duration' => 'Duración',
            'total' => 'Total',
            'inserted' => 'Insertados',
            'updated' => 'Actualizados',
            'deleted' => 'Eliminados',
            'finalized' => 'Finalizado',
            'process' => 'En proceso',
            'errors' => 'Errores',
            'export' => 'Exportar'
        ]
    ]
];
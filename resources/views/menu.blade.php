{{-- links de menú para incluir en list-group --}}
@component('components.menu.item', [
         'route' => route('csw-harvesting::csw.index', $instance),
         'image' => asset('img/menu-icons/csw-external.png'),
         'text' => __('csw-harvesting::common.menu.title'),
         'details' => __('csw-harvesting::common.menu.details')
     ])
@endcomponent

@extends('layouts.app')

@section('content')
    <div class="container"  id="app">
        {{--}}<div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('csw::csw.show', $csw->id) !!}
            </div>
        </div>{{--}}

        @component('components.panel.simple', ['title' => $csw->name, 'flash' => true])
                <div class="table-responsive">
                    <table class="table show-table" style="margin-bottom: 0">
                        <thead class="show-table-header">
                        <tr>
                            <td class="show-table-label">{{ __('csw-harvesting::common.csw.fields.name') }}</td>
                            <td class="show-table-data">
                                {{ $csw->name }}
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="show-table-label">{{ __('csw-harvesting::common.csw.fields.url') }}</td>
                            <td class="show-table-data">
                                {{ $csw->url }}
                            </td>
                        </tr>
                        <tr>
                            <td class="show-table-label">{{ __('csw-harvesting::common.csw.fields.cron') }}</td>
                            <td class="show-table-data">
                                {{ $csw->cron }}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
        @endcomponent

        @component('components.panel.toolbar', ['title' => __('csw-harvesting::common.csw.record.title')])
            @slot('toolbar')
                {{-- <a href="{{ route('csw-harvesting::csw.harvest.retry', [$instance, $csw->id,] ) }}" class="btn btn-sm btn-default" title="{{__('csw-harvesting::common.show.retry')}}">
                    <span>{{ __('csw-harvesting::common.show.retry') }}</span>
                </a> --}}
            @endslot

            <table class="table">
                <thead>
                <tr>
                    <th>{{ __('csw-harvesting::common.csw.record.status') }}</th>
                    <th>{{ __('csw-harvesting::common.csw.record.total') }}</th>
                    <th>{{ __('csw-harvesting::common.csw.record.inserted') }}</th>
                    <th>{{ __('csw-harvesting::common.csw.record.updated') }}</th>
                    <th>{{ __('csw-harvesting::common.csw.record.deleted') }}</th>
                    <th>{{ __('csw-harvesting::common.csw.record.errors') }}</th>
                    <th class="text-right">
                        <pagination-header :data="{{ $collection->toJson() }}"></pagination-header>
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($collection as $element)
                        <tr>
                            <td>
                            <span class="glyphicon {{ $element->getIcon() }}"></span>
                                {{ __('csw-harvesting::common.csw.record.'.$element->status()) }}
                            </td>
                            <td>{{ $element->total }}</td>
                            <td>{{ $element->inserted }}</td>
                            <td>{{ $element->updated }}</td>
                            <td>{{ $element->deleted }}</td>
                            <td>
                                @if( $element->errors != null)
                                    <a href="{{ route('csw-harvesting::csw.harvest.export', [$instance, $element->id]) }}"
                                        class="btn btn-info btn-xs">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <span class="hidden-xs">{{__('csw-harvesting::common.csw.record.export')}}</span>
                                    </a>
                                @endif
                            </td>
                            <td style="padding: 2px 8px;" data-datetime="{{ $element->created_at}}">
                                <div>
                                    <span class="glyphicon glyphicon-calendar"></span> {{ ucfirst($element->initiated()) }}
                                </div>
                                @if( $element->updated_at != null)
                                    <div>
                                        <span class="glyphicon glyphicon-time"></span> {{ $element->duration() }}
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $collection->links() }}
        @endcomponent
    </div>
@endsection
@include('aod::partials-views.clipboard')
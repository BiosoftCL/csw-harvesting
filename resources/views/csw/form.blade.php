<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', __('csw-harvesting::common.csw.fields.name'), ['class' => 'col-sm-4 control-label required']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
    {!! Form::label('name', __('csw-harvesting::common.csw.fields.url'), ['class' => 'col-sm-4 control-label required']) !!}
    <div class="col-sm-6">
        {!! Form::text('url', null, ['class' => 'form-control']) !!}
        {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
    </div>
</div>
{{-- <div class="form-group {{ $errors->has('cron') ? 'has-error' : ''}}">
    {!! Form::label('name', __('csw-harvesting::common.csw.fields.cron'), ['class' => 'col-sm-4 control-label required']) !!}
    <div class="col-sm-6">
        {!! Form::text('cron', null, ['class' => 'form-control']) !!}
        {!! $errors->first('cron', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}

<hr class="row">
<div class="center">
    <div class="">
        <p class="">
           {{ __('csw-harvesting::common.cron.cron_section_title') }}
        </p>
    </div>

    <div class="row">
        <div class="col-sm-3">
           <strong>{{ __('csw-harvesting::common.cron.minutes_title_section') }}</strong>
            <div class="radio">
                <label>
                    <input type="radio" value="1" name="frequencyOption"  {!!isset($seed) && $seed->frequency == 1  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.everyMinute') }}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="2" name="frequencyOption" {!!isset($seed) && $seed->frequency == 2  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.everyFiveMinutes') }}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="3" name="frequencyOption" {!!isset($seed) && $seed->frequency == 3  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.everyTenMinutes') }}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="4" name="frequencyOption" {!!isset($seed) && $seed->frequency == 4  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.everyFifteenMinutes') }}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="5" name="frequencyOption" {!!isset($seed) && $seed->frequency == 5  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.everyThirtyMinutes') }}
                </label>
            </div>
        </div>
        <div class="col-sm-3">
            <strong>{{ __('csw-harvesting::common.cron.hours_title_section') }}</strong>
            <div class="radio">
                <label>
                    <input type="radio" value="6" name="frequencyOption" {!!isset($seed) && $seed->frequency == 6  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.hourly') }}
                </label>
            </div>
        </div>
        <div class="col-sm-6">
            <strong>Días</strong>
            <div class="radio">
                <label>
                    <input type="radio" value="7" name="frequencyOption" {!!isset($seed) && $seed->frequency == 7  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.daily') }}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="8" name="frequencyOption" style="margin-top: 8px" {!!isset($seed) && $seed->frequency == 8  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.dailyAt') }}
                    {!! Form::time('first_hour_daily_at', null) !!}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="9" name="frequencyOption" style="margin-top: 8px" {!!isset($seed) && $seed->frequency == 9  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.twiceDaily') }}
                    {!! Form::select('first_hour_twice_daily',[
                        "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23",
                    ],null,['class' => 'form-control', 'style' => "display: inline-block; width: auto"]) !!}
                    {{ __('csw-harvesting::common.cron.between_hours') }}
                    {!! Form::select('second_hour_twice_daily',[
                        "00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23",
                    ],null,['class' => 'form-control', 'style' => "display: inline-block; width: auto"]) !!}
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <strong>{{ __('csw-harvesting::common.cron.weeks_title_section') }}</strong>
            <div class="radio">
                <label>
                    <input type="radio" value="10" name="frequencyOption" {!!isset($seed) && $seed->frequency == 10  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.weekly') }}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="11" name="frequencyOption" style="margin-top: 10px" {!!isset($seed) && $seed->frequency == 11  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.weeklyOn') }}

                    {!! Form::select('weekday_weekly_on',[
                        1 => __('csw-harvesting::common.cron.monday'),
                        2 => __('csw-harvesting::common.cron.tuesday'),
                        3 => __('csw-harvesting::common.cron.wednesday'),
                        4 => __('csw-harvesting::common.cron.thursday'),
                        5 => __('csw-harvesting::common.cron.friday'),
                        6 => __('csw-harvesting::common.cron.saturday'),
                        0 => __('csw-harvesting::common.cron.sunday'),
                    ],null,['class' => 'form-control', 'style' => "display: inline-block; width: auto"]) !!}
                    {{ __('csw-harvesting::common.cron.hour_option') }}
                    {!! Form::time('first_hour_weekly_on', null) !!}
                </label>
            </div>
        </div>
        <div class="col-sm-6">
            <strong>{{ __('csw-harvesting::common.cron.months_title_section') }}</strong>
            <div class="radio">
                <label>
                    <input type="radio" value="12" name="frequencyOption" {!!isset($seed) && $seed->frequency == 12  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.monthly') }}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="13" name="frequencyOption" style="margin-top: 10px" {!!isset($seed) && $seed->frequency == 13  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.monthlyOn') }}
                    {!! Form::select('day_monthly_on', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                    null,['class' => 'form-control', 'style' => "display: inline-block; width: auto"]) !!}
                    {{ __('csw-harvesting::common.cron.hour_option') }}
                    {!! Form::time('first_hour_monthly_on', null) !!}
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" value="14" name="frequencyOption" {!!isset($seed) && $seed->frequency == 14  ? "checked" : "" !!}>
                    {{ __('csw-harvesting::common.cron.quarterly') }}
                </label>
            </div>
        </div>
    </div>
</div>
<hr class="row">

@extends('layouts.app')

@section('content')

    <div class="container">
        @component('components.panel.simple', ['title' => __('csw-harvesting::common.create.title')])
            {!! Form::open(['route' => ['csw-harvesting::csw.store', $instance], 'class' => 'form-horizontal']) !!}

            @include ('csw-harvesting::csw.form')
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="pull-right">
                            {!! Form::submit(trans('form.create_submit'), ['class' => 'btn btn-primary btn-submit-disable', 'data-loading-text' => trans('form.create_submit')]) !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        @endcomponent

    </div>

@endsection
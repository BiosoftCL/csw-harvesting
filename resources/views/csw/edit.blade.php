@extends('layouts.app')

@section('content')

    <div class="container">

        {{--}}<div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('csw::csw.edit', $csw->id) !!}
            </div>
        </div>{{--}}

        @component('components.panel.simple', ['title' => __('csw-harvesting::common.edit.title')])
            {!! Form::model($seed, [
                'method' => 'PATCH',
                'route' => ['csw-harvesting::csw.update', $instance, $seed],
                'class' => 'form-horizontal'
            ]) !!}

            @include ('csw-harvesting::csw.form')
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="pull-right">
                            {!! Form::submit(__('csw-harvesting::common.edit.edit_btn'), ['class' => 'btn btn-primary btn-submit-disable', 'data-loading-text' => __('csw-harvesting::common.edit.edit_btn')]) !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
        @endcomponent

    </div>

@endsection
@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        {{--}}<div class="row">
            <div class="col-md-10 col-md-offset-1">
                {!! Breadcrumbs::render('csw::csw') !!}
            </div>
        </div>{{--}}

        @component('components.panel.toolbar', ['title' => __('csw-harvesting::common.index.title'), 'flash' => true])
            @slot('toolbar')
                <a href="{{ route('csw-harvesting::csw.create', $instance) }}" class="btn btn-sm btn-default" title="{{__('csw-harvesting::common.index.new_btn')}}">
                    <span>{{ __('csw-harvesting::common.index.new_btn') }}</span>
                </a>
            @endslot

            <table class="table">
                <thead>
                <tr>
                    <th>{{ __('csw-harvesting::common.csw.fields.name') }}</th>
                    <th>{{ __('csw-harvesting::common.csw.fields.url') }}</th>
                    <th>{{ __('csw-harvesting::common.csw.fields.cron') }}</th>
                    <th class="text-right">
                        <pagination-header :data="{{ $collection->toJson() }}"></pagination-header>
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($collection as $element)
                        <tr>
                            <td>{{ $element->name }}</td>
                            <td>{{ $element->url }}</td>
                            <td>{{ $element->cron }}</td>
                            <td class="text-right text-nowrap">
                                <a href="{{ route('csw-harvesting::csw.show', [$instance, 'id' => $element->id]) }}"
                                   class="btn btn-sm btn-default" title="{{ __('form.view') }}">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                </a>
                                <a href="{{ route('csw-harvesting::csw.edit', [$instance, 'id' => $element->id]) }}"
                                   class="btn btn-sm btn-default" title="{{ __('form.edit') }}">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => route('csw-harvesting::csw.destroy', [$instance, $element->id]),
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="glyphicon glyphicon-trash" aria-hidden="true"></i>', [
                                    'type' => 'submit',
                                    'class' => 'btn btn-sm btn-danger',
                                    'title' => __('csw-harvesting::common.csw.actions.delete'),
                                    'onclick'=>'return confirm("'.__('csw-harvesting::common.csw.actions.delete_confirm', ['foo' => $element->bar]).'")'
                                ]) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $collection->links() }}
        @endcomponent

    </div>
@endsection
# Skeleton

Paquete de ejemplo para ambiente Geonodo

## Estructura


```
config/
database/
    migrations/
resources/
    assets/
    lang/
    views/
routes/
src/
    Http/
        Controllers/
        Request/
```


## Instalación en producción

Añadir en archivo `composer.json`

```json
{
    "require": {
        "geonodo/csw-harvesting": "^1.0"
    },

    "repositories": [
        {
            "type": "git",
            "url": "git@bitbucket.org:BiosoftCL/csw-harvesting.git"
        }
    ]
}
```

Instalar paquete

```bash
composer update
```

Publicar assets

```bash
php artisan vendor:publish --provider="Geonodo\CSWHarvesting\CSWHarvestingServiceProvider" --force 
```

Migrar tablas de datos

```bash
php artisan migrate
```

## Registro de cambios

Ver [CHANGELOG](CHANGELOG.md).
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes prefix
    |--------------------------------------------------------------------------
    |
    | Texto prefijo para las rutas del paquete
    |
    */

    'routes_prefix' => 'production/csw-harvesting',

    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Middleware por los que filtrarán las rutas del paquete
    |
    */

    'middleware' => ['web', 'auth', 'role:editor', 'session.instance'],

    /*
    |--------------------------------------------------------------------------
    | Routes prefix API
    |--------------------------------------------------------------------------
    |
    | Texto prefijo para las rutas del paquete
    |
    */
    'routes_prefix_api_v1' => 'csw-harvesting/api',

    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Middleware por los que filtrarán las rutas del paquete
    |
    */
    'middleware_api_v1' => ['cors'],

];

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePkgCwsharSiloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkg_cswharvest_silo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('identifier');
            $table->text('profile');
            $table->integer('source_id')->unsigned()->index();
            $table->foreign('source_id')->references('id')->on('pkg_cswharvest_seeds')->onDelete('cascade');
            $table->text('xml');
            $table->timestamp('datestamp', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkg_cswharvest_silo');
    }
}

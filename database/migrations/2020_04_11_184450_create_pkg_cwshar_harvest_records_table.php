<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePkgCwsharHarvestRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkg_cswharvest_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('source_id')->unsigned()->index();
            $table->foreign('source_id')->references('id')->on('pkg_cswharvest_seeds')->onDelete('cascade');
            $table->integer('total')->unsigned();
            $table->integer('inserted')->unsigned()->nullable();
            $table->integer('updated')->unsigned()->nullable();
            $table->integer('deleted')->unsigned()->nullable();
            $table->text('errors')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkg_cswharvest_records');
    }
}
